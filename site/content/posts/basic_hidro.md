---
title: Armate un hidro con lo mínimo indispensable
date: 2020-03-29T14:30:27-03:00
lastmod: 2020-03-29T14:30:27-03:00
author: Atneucanon
# cover: /img/cover.jpg
categories: ["hidro"]
tags: ["hidro", "beginner"]
# showcase: true
draft: false
---

## Corta la bocha

Definitivamente si estás queriendo incursionar en hidro por primera vez, te aconsejo ir por DWC. En el siguiente post voy a describir cosas a tener en cuenta y costos para armarte un hidro decente.

Debido a que en Argentina los precios varían mucho, voy a poner el costo en U$S _blue_ ( 😉 ), para tener un mejor parámetro de los costes.

## MoneyMoneyMoney

Armar un hidro no es _tan_ caro, pero el tema es que tiene un costo adicional respecto a los cultivos en sustrato, particularmente por el tipo de químicos y demás _exquisiteses_ que este sistema puede requerir.

Se pueden armar hidros hogareños completos (cabina incluída) a partir de 250~300 U$S. A partir de los 1000 U$S, ya lo consideraremos de mediana escala y por consiguiente, se deben cumplir normas más estrictas, particularmente con lo que es la instalación eléctrica.

## Recursos e Inventario por categoría

### Buckets

Si sos vago, unos buckets ya armados, pueden rondar los **40 U$S** cada uno los básicos. 

En el caso de que te los armes vos, tené en cuenta los siguientes requisitos:

- Recipiente resistente y lo más opaco posible.
- Preferentemente, recubrir el recipiente con aislante.
- Piedras de río, traetelas de donde puedas, los acuarios te surten 20 U$S por las piedras de acuario. Cumplen un rol fundamental para albergar organismos, ahorro de químicos (principalmente en la primera fase de vegetación) y regulación de temperatura.
- Customizar los buckets puede llevar tiempo y dinero, pero a grandes rasgos no más de 10 U$S por cada uno para agregar adicionales.
  - Leca, por ejemplo. Para poner en la maceta de red y completar el espacio. En casas grandes de construcción se puede conseguir leca plástica, la más recomendable.
  - Los Jiffys(*) de coco suelen ser caros y algo difíciles de conseguir, pero no es algo que supere los pocos dólares la unidad.   
- Una bomba de agua puede andar entre los 12 y 20 U$S. Bombas más caras no son necesarias para buckets pequeños.
- Aireador más los extremos pueden rondar los 15 U$S.

> Estos son los Jiffys de coco:

![Jiffys](/img/content/posts/jiffys.jpg "Jiffys")

Para que te des una idea, el bucket internamente queda así:

![Bucket](/img/content/posts/bucket.png "Bucket")


### Luz

Si el espacio permite, el ruido y el calor no son un factores que afecten la instalación, Sodio es una opción económica y altamente productiva. De modo contrario, el _LED_ genera menos calor y es más eficiente en el uso del espacio, además de mayor eficiencia en consumo de energía (lo que no quiere decir que gaste poco).

Acá se viene la inversión fuerte. Un LED bueno puefe irse por encima de los **250 U$S** fácilmente, pero su vida útil amerita su coste. El desgaste del sodio, requiere renovar las lámparas luego de cierto tiempo de uso. 


### Agua

El agua potable contiene una alta conductividad, además de contener Cloro, un componente que puede afectar el estado de la raíz y consecuentemente reducir el rendimiento del cultivo.

Un purificador de agua es lo más recomendable y puede ir por encima de los 120 U$S. Lo bueno, es que este purificador es de uso hogareño y sirve también para consumo. Estos aparatos tienen una vida útil muy extensa, incluso los más pequeños pueden purificar +15k litros de agua. 


### Ventilación

- Extractor. Crucial, aunque se pueden utilizar los _fan_ de 220v, se recomienda un extractor bueno. > 60US$S
- Ventilador con sufuciente potencia como para llegar a todas las plantas.
- Caloventor/Calefactor/Aire. Existen aparatos económicos para regular la temperatura de espacios chicos. En mi caso particular, durante el verano necesito usar un Ventilador/Acondicionador, que reduce la temperatura en 1 o 2 grados Celsius -- no parece mucho, pero es suficiente en la mayor cantidad de los casos.

### Químicos

Esto requiere un apartado dedicado, porque de por sí las medidas y aplicaciones varían, así como la calidad y la disponibilidad.

Lo que mayormente me recomiendan -- y recomiendo --, aunque no es lo más económico, es [**Powder Feeding**](https://listado.mercadolibre.com.ar/powder-feeding?matt_tool=6170046&matt_word=POWDER_FEEDING&gclid=Cj0KCQjwjoH0BRD6ARIsAEWO9DuqV5TPoqxreFEmD4iF-_hyxnjRwrLZchBGLQVvx8VXLtKLZ9CzpD4aAnRMEALw_wcB). Entre lo nacional, vengo usando [Fácil Cultiva](https://facilcultiva.com/), en especial en período vegetativo.

> Sumamente importante: no todos los químicos son aptos para hidroponia. 

> Todos los químicos adicionales (boosters, delta, etc), se aplican foliarmente y durante la noche.

### Varios

Entre los _varios_, se pueden encontrar las siguientes cosas:

- Tubos y mangueras.
- Capa aislante de aluminio/plástica. Hay varios grosores, no escatimes.
- Zapatillas eléctricas
- Disyuntor o llave, lo que te permita _apagar_ el indoor en caso de emergencia y sobreutilización del sistema eléctrico.

## Adios

¡Espero que les haya servido! 

!["Adios"](/img/content/posts/adios.jpg "Adios")