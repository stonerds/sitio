---
title: ¡Hola, Ents!
date: 2020-03-28T15:35:46-03:00
lastmod: 2020-03-28T15:35:46-03:00
author: Atneucanon
cover: /img/cover.png
categories: ["general"]
tags: ["General"]
# showcase: true
draft: false
---

Con mis exepcionales capacidades de robos de themes públicos, he dado por iniciado este humilde y escueto log al servicios de los Ents interesados en el autocultivo.

Tanto si estás iniciando o ya sos re-capo, puede que encuentres cosas en común, que te pueden ayudar o también, que cosas tengo que mejorar yo :P

Actualmente me encuentro experimentando con [Hidroponía](https://es.wikipedia.org/wiki/Hidropon%C3%ADa) en distintos formatos -- [NFT](https://en.wikipedia.org/wiki/Nutrient_film_technique) y [DWC](https://en.wikipedia.org/wiki/Deep_water_culture) --, con buenos resultados en ambos.

¡Sin más, les doy la bienvenida!
