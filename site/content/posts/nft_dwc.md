---
title: Experiencias compartidas con NFT y DWC
date: 2020-03-28T18:01:30-03:00
lastmod: 2020-03-28T18:01:30-03:00
author: Atneucanon
# cover: /img/content/posts/cover_dwc_nft.png
categories: ["hidro"]
tags: ["hidro", "NFT", "DWC"]
# showcase: true
draft: false
---

## Experiencias mixtas

Si bien incursioné en el cultivo del cannabis en sustrato y con semillas de prensado, llevo ya un tiempo experimentando con la hidroponia en varios formatos y aún me quedan cosas que experimentar 😈

Por el momento, he experimentado con 2.5 tipos de [Hidroponía](https://es.wikipedia.org/wiki/Hidropon%C3%ADa):

- El _Deep Water Culture_ ([DWC](https://en.wikipedia.org/wiki/Deep_water_culture)) que consiste en sumergir las raíces en el medio en un recipiente que permita aislar la ráiz de la luz y en una temperatura no mayor a los 20/21 grados -- que en la práctica se traduce en algunas técnicas que comentaré en próximos posts.

![DWC ](/img/content/posts/dwc.jpg "DWC ")

- [NFT](https://en.wikipedia.org/wiki/Nutrient_film_technique) o _Nutrient Film Technique_, es un sistema de tubos donde el agua circula desde un contenedor central.

![NFT](/img/content/posts/nft.jpg "NFT")

- El sistema NFT, terminó utilizando _sistema de goteo_, debido a que el diámetro de los tubos no permitía el acceso a la capa de nutrientes cuando uno sale de semilla. Por lo tanto, no era estríctamente un NFT, sino un híbrido.

## DWC y variaciones

Mi instalación es muy simplista, porque no hay conexión entre los _buckets_. Esto podría servir para poner un desagote único, con un contenedor central y así tener la ventaja del NFT respecto al consumo de nutrientes. 

Sin embargo, esto no permite individualizar las proporciones de químicos para cada tipo de planta (_Long_ o _Short_ flowering), algo que puede impactar en el rendimiento del cultivo. 


## Lo bueno del NFT

- Permite mayor variedad, por permitir una distribución eficiente en relación al espacio de base. Para una idea, en un espacio de 1x1.6mts, llegué a tener algo de 7 plantas, de mediano tamaño, pero buena calidad.
- Se ahorra mucho químico, ya que uno centraliza los nutrientes en un solo recipiente que mantiene una circulación.
- Queda re-pro cuando vienen visitas y flashean Breaking Bad mal.

## Lo poronga del NFT

- En Argentina, con los cortes de luz, se hace muy difícil que el sistema sea resiliente. A menos que tengas un generador, podés llegar a tener mucha pérdida si también otros factores como el calor, entran en juego durante un considerable tiempo. Esto no te permite ausentarte mucho de la instalación.
- Requiere mucho mantenimiento, por poseer juntas, tuberías, mangueras, etc. 
- La producción es buena, pero no se compara con la de DWC. Las raíces "compiten" en la capa de nutrientes y están más expuestas a la temperatura.  
- Es más caro _relativamente_ (ver [Lo choto del DWC](#lo-choto-del-dwc) ).

## Lo bueno del DWC

- Para empezar, es el más sencillo.
- La mayoría lo usa por ser simple, práctico y con buena producción.
- Las raíces, al permanecer en el medio, hace que el sistema sea a prueba de cortes de luz.
- Es más fácil aislar la raíz de la luz directa. Los caños del NFT suelen filtrar luz, lo que afecta la raíz.

## Lo choto del DWC

- No es un defecto, pero requiere mucho espacio vertical, ya que la planta crece _mucho_. Yo estimo que el tamaño varía 1.3x con respecto a sustrato.
- No es económica la instalación, pero es más económica que el NFT. Pero, si tenés todos los _buckets_ con aireador y bomba, el sistema se encarece.
- No es tan cheto como el NFT (¿?). 


No fumen del pico, gracias.

