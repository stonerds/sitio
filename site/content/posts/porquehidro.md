---
title: ¿Por qué Hidroponia para mi flor?
date: 2020-03-28T17:38:05-03:00
lastmod: 2020-03-28T17:38:05-03:00
author: Atneucanon
cover: /img/content/posts/groso.jpg
categories: ["hidro"]
tags: ["hidro"]
showcase: true
draft: false
---

_La imagen del cover no es mi setup, ojalá._

## Hidroponia y Cannabis: ❤️

> Terminología: [Hidroponía](https://es.wikipedia.org/wiki/Hidropon%C3%ADa)

En la comunidad de [Ents](https://www.reddit.com/r/ArgEntos), varios consultaron acerca de este sistema que cada vez comienza a ser más accesible al público en general, aunque no carezca de cierta dificultad para el armado y el mantenimiento.

Dentro de la hidroponia misma, existen distintos tipos de instalaciones, con sus defectos y ventajas, pero en este post quiero focalizarme en el análisis comparativo basado en mi experiencia personal y la de referencias de otros cultivadores. Incluso, ni siquiera es el sistema que garantiza mayor produccción, cuyo podio queda para la _aeroponia_, pero que hoy por hoy no es algo muy práctivo para el cultivo de baja escala u hogareña.

## El viejo y sexy sustrato

Debo confesar que extraño el sustrato cuando se refiere a practicidad al momento de armar la instalación y, en el sabor, aunque en menor grado. 

La otra cosa que el sustrato tiene a favor, es que se facilita el doble si tenés lugar para cultivo de exterior, porque un sistema hidropónico de exterior requiere una bomba de agua más potente y aislar el tanque de agua de manera más cautelosa -- se puede armar un pozo bajo tierra con tapa, para quitar algunos grados de temperatura.

El otro, es que es mucho más fácil (en tiempo y recursos) de explotar al máximo el tamaño de la planta si es germinada en tiempo correcto.

Donde el cultivo en sustrato se hace más inviable es en _indoor_, debido a que no solo hay que programar los riegos, sino que también programar riegos con nutrientes, además de tener un buen desagote de macetas, de modo contrario el _forrosarium_ puede aparecer o incluso matar a la planta por exeso de agua (ouch). También, nos agrega limitante en el crecimiento de raíces y la capacidad del recipiente es un limitante importante en el crecimiento de la planta. 

Es ahí donde la Hidroponia le pasa al trapo al sustrato.

## Alto hidro, bruh

Específicamente, ¿donde está la ventaja?:

- Producción
  - El tamaño de las plantas es considerable, voy a escribir de esto particularmente, porque también me supuso un problema en su momento.
- Tiempo
  - Esto se aprecia desde el día 1. La planta cambia _todos_ los días.
- Higiene
  - Al ser el agua el único medio de la planta, requiere extensivos cuidados. Esto también obliga al cultivador a ser más ordenado.
- Entrenamiento
  - La hidroponia exije que conozcas más a tus plantas y el impacto de la calidad de los químicos.

Acá les dejo la foto de mi último setup, de tipo DWC con Leds mixtos de 600 y  300 watts:


![DWC Setup](/img/content/posts/setup.jpg "DWC Setup")


## Pero ...

Los _peros_ son muchos, aunque las _peras_ sean buenas (¿?):

- Mantenimiento
  - Dependiendo el sistema -- y como todo sistema que usa al solvente universal --, se deteriora, genera pérdidas, etc. Si bien el mantenimiento de la planta en si es igual o incluso menor que en "tierra", la complejidad de la instalación puede traerte dolores de cabeza.
- Coste
  - No es tanto el coste del sistema hidropónico en si, sino que debido a que el agua no tiene que superar determinada temperatura, requiere mayor ventilación o sistemas de refrigeración. De esto deriva que si el espacio es chico, es muy posible que luz de Sodio/Mercurio no sea una buena opción, lo que lleva a tener LED y consiguientemente mayor 💸.
- Tiempo de instalación
  - Lleva tiempo levantar un cultivo hidropónico y lo mejor que podés hacer es planearlo bien, debido a que al requerir mayor mantenimiento, tenés que ser práctico y pragmático a la hora de pensar "como vas a arreglar esto" , "como voy a cambiar eso" o "que pasa si la bomba deja de funcionar".
- Tratamiento del Agua
  - Es todo un tema el tratamiento del agua y los químicos que uses. El uso de un medidor de PH y de EC se hacen imprescindibles. Para el agua, se recomienda usar purificada (debido al alto contenido de cloro).

## Albahaca en experimento

Para que tengan una idea de la diferencia de rendimiento entre sustrato e hidroponia, voy a mostrar una hoja de la misma planta de albahaca, en cada sistema.

El especimen del sistema hidropónico es un esqueje de una planta en tierra, con cuidados normales.

Esta es la hoja en el especimen en sustrato:

![albahaca_tierra ](/img/content/posts/albahaca_tierra.jpg "albahaca_tierra")

Esta es la hoja en el sistema hidropónico:

![albahaca_hidro](/img/content/posts/albahaca_hidro.jpg "albahaca_hidro")

Como apreciarán, la diferencia es importante. No solo en tamaño, sino en la estructura, color uniforme más oscuro y hojas más resistentes.

Miren sino, esta preciosa hoja de Gorilla Glue de BSF Seeds:

![gorilla_glue_leaf](/img/content/posts/gorilla_glue_leaf.jpg "gorilla_glue_leaf")